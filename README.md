Here is a basic calculator function with UI. Now it is your turn to enhance:

- CSS
- responsive style
- themes (light/dark)
- Story Book
- analog interaction (QR/NFC)

–

- tests
- i18n
- debounce
- routing
- code splitting

–

- table
- (csv) export
- drag'n'drop files (upload)
- drag'n'drop elements
- lazy loading / infinite scroll / virtual scroll

–

- ES-Lint
- TypeScript
- (async) API
- error reporting

–

- versioning
- PWA
- CI
- native
