import React from "react";
import { pert, isNumberLike } from "../../service/util";

class PertUI extends React.Component {
  constructor(props) {
    super(props);
    this.form = null;
    this.resultEmpty = "–";
    this.state = {
      result: this.resultEmpty
    };
  }

  componentDidMount() {
    this.handleFormChange();
  }

  sanitizeInputs() {
    let { optimistic, normal, pessimistic } = this.form.elements;
    function sanitize(input) {
      if (!isNumberLike(input.value)) input.value = 1;
    }
    sanitize(optimistic);
    sanitize(normal);
    sanitize(pessimistic);
  }

  handleFormChange = event => {
    let { optimistic, normal, pessimistic } = this.form.elements;

    if (!event || event.type === "blur") this.sanitizeInputs();

    normal.min = optimistic.value;
    pessimistic.min = Math.max(optimistic.value, normal.value);

    let result = pert(optimistic.value, normal.value, pessimistic.value);

    if (!isNumberLike(result)) result = this.resultEmpty;
    this.setState({ result });
    console.log("form valid:", this.form.checkValidity());
  };

  render() {
    const { result } = this.state;

    return (
      <div>
        <form
          className="pert-calculator"
          ref={element => (this.form = element)}
          onChange={this.handleFormChange}
          onBlur={this.handleFormChange}
        >
          <div>
            <label htmlFor="optimistic">
              optimistic
              <input name="optimistic" type="number" min="1" step="1" />
            </label>
          </div>

          <div>
            <label htmlFor="normal">
              normal
              <input name="normal" type="number" min="1" />
            </label>
          </div>

          <div>
            <label htmlFor="pessimistic">
              pessimistic
              <input name="pessimistic" type="number" min="1" />
            </label>
          </div>
        </form>

        <h2>{result}</h2>
      </div>
    );
  }
}

export default PertUI;
