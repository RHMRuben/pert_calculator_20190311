# What it does / Expectation

- **input onchange**: It automatically calculates the result
  - if the value is invalid (e.g. empty string) the resuls shows `"–"`
  - input validation _(html5)_: `normal >= optimistic`; `pessimistic >= normal`
- **input onblur**: It sanitizes invalid values (e.g. not-numbers) to `1`
