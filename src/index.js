import React from "react";
import ReactDOM from "react-dom";
import PertUI from './components/pert-ui';

import "./styles.css";

function App() {
  return (
    <div className="App">
      <h1>PERT Calculator</h1>
      <h2>Time estimations</h2>
      <PertUI />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
