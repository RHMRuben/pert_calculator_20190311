export function isNumberLike(x) {
  return x != null && x !== "" && !isNaN(x);
}

export function pert(optimistic, normal, pessimistic, accuracy = 2) {
  if (
    !isNumberLike(optimistic) ||
    !isNumberLike(normal) ||
    !isNumberLike(pessimistic)
  ) {
    return;
  }
  const factor = Math.pow(10, accuracy);
  const result =
    (parseFloat(optimistic) +
      parseFloat(normal) * 4 +
      parseFloat(pessimistic)) /
    6;
  return Math.round(result * factor) / factor;
}
